// import express
const express = require('express');
// import model 
const OrderModel = require('../model/OrderModel')
const CustomerModel = require('../model/CustomerModel')
// import mongoose
const mongoose = require('mongoose');
const { create } = require('../model/OrderModel');
// tạo post 
const createOrderOfCustomer = (request, response) => {
    // thu thập dữ liệu
    let body = request.body;
    let customerId = request.params.customerId;
    // validate
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "customer ID is invalid"
        })
    }
    if (!body.shippedDate) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "shippedDate is required"
        })
    }
    if (!body.orderDetail) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "orderDetail is required"
        })
    }
    if (!body.cost) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "cost is required"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let orderModelData = {
        _id: mongoose.Types.ObjectId(),
        shippedDate: body.shippedDate,
        orderDetail: body.orderDetail,
        cost: body.cost,
    }
    OrderModel.create(orderModelData, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            CustomerModel.findByIdAndUpdate(customerId, {
                $push: { orders: data._id }
            },
                (err, updatedCourse) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return response.status(201).json({
                            status: "Create order Success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}
// get all orders of customer
const getAllOrderOfCustomer = (request, response) => {
    // b1 lấy dữ liệu
    let customerId = request.params.customerId;
    // validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "order Id is not valid"
        })
    }
    // b3 sử dụng cơ sơ dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    CustomerModel.findById(customerId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get all order of customer success",
                    data: data.orders
                })
            }
        })
}
// tạo get all  
const getAllOrder = (request, response) => {
    OrderModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all orders successfully",
                data: data
            })
        }
    })
}
// tạo get by id 
const getOrderById = (request, response) => {
    // lấy param 
    let ordersId = request.params.ordersId;
    //B2 : Validate
    if (!mongoose.Types.ObjectId.isValid(ordersId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "order Id is not valid"
        })
    }
    // B3: Thao tắc với cơ sở dữ liệu
    OrderModel.findById(ordersId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get orders by id success" + ordersId,
                data: data
            })
        }
    })
}
// tạo post 
const updateOrder = (request, response) => {

    // thu thập dữ liệu
    let ordersId = request.params.ordersId;
    let body = request.body;
    // validate
    if (!body.shippedDate) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "shippedDate is required"
        })
    }
    if (!body.orderDetail) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "orderDetail is required"
        })
    }
    if (!body.cost) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "cost is required"
        })
    }
    // b3 sử dụng cơ sở dữ liệu
    let orderupdate = {
        shippedDate: body.shippedDate,
        orderDetail: body.orderDetail,
        cost: body.cost,
    }
    OrderModel.findByIdAndUpdate(ordersId, orderupdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update orders type success",
                data: data
            })
        }
    })
}
// tạo post 
const deleteOrder = (request, response) => {
    // B1: thu thập dữ liệu
    let ordersId = request.params.ordersId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(ordersId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "orders Id is not valid"
        })
    }

    //B3: Thao tắc với cơ sở dữ liệu
    OrderModel.findByIdAndDelete(ordersId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update orders success" + ordersId + " Success ",
            })
        }
    })
}
// tạo order cho customer
const createCustomerOrder = (request, response) => {
    let body = request.body
    // sử dụng cơ sơ dữ liệu
    CustomerModel.findOne({
        email: request.query.email
    }, (errorFindUser, userExist) => {
        // nếu xảy ra lỗi thì trả về lỗi 500
        if (errorFindUser) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindUser.message
            })
        } else {
            // nếu không tồn tại thì tạo user mới và order mới
            if (!userExist) {
                // tạo user mới
                response.status(500).json({
                    status: "Error 500: Internal sever Error",
                    message: "Email customer không hợp lệ!!"
                })
            } else {
                OrderModel.create({
                    _id: mongoose.Types.ObjectId(),
                    shippedDate: body.shippedDate,
                    orderDetail: body.orderDetail,
                    cost: body.cost,
                    fullName: userExist.fullName,
                    email: userExist.email,
                    phone: userExist.phone,
                }, (error, orderData) => {
                    if (error) {
                        response.status(500).json({
                            status: "Error 500: Internal sever Error",
                            message: error.message
                        })
                    } else {
                        CustomerModel.findByIdAndUpdate(userExist._id,
                            { $push: { orders: orderData._id } }, (error, userData) => {
                                response.status(200).json({
                                    status: "Success: create orders success",
                                    data: {
                                        _id: userExist._id,
                                        shippedDate: orderData.shippedDate,
                                        orderDetail: orderData.orderDetail,
                                        cost: orderData.cost,
                                        fullName: userExist.fullName,
                                        email: userExist.email,
                                        phone: userExist.phone,
                                    }
                                })
                            })
                    }
                })
            }
        }
    })
}

module.exports = {
    createCustomerOrder,
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrder,
    deleteOrder
}